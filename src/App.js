import logo from './logo.svg';
import './App.css';
import Phone_Redux from './Phone_redux/Phone_Redux';

function App() {
  return (
    <div className="App">
      <Phone_Redux />
    </div>
  );
}

export default App;
