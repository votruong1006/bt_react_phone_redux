import React, { Component } from "react";
import { connect } from "react-redux";

class Detail_Phone extends Component {
    render() {
        let { maSP, tenSP, manHinh, heDieuHanh, cameraTruoc, cameraSau, ram, rom, giaBan, hinhAnh } = this.props.detail
        return (
            <div className="row">

                <div className="col-4 p-4">
                    <img className="bg-primary" style={{ width: "100%" }} src={hinhAnh} alt="" />
                </div>
                <div className="col-8 p-4">
                    <h2>{maSP}</h2>
                    <h2>{tenSP}</h2>
                    <h2>{manHinh}</h2>
                    <h2>{heDieuHanh}</h2>
                    <h2>{cameraTruoc}</h2>
                    <h2>{cameraSau}</h2>
                    <h2>{ram}</h2>
                    <h2>{rom}</h2>
                    <h2>{giaBan}</h2>
                </div>


            </div>
        );
    }
}
let mapStateToProps = (state) => {
    return { detail: state.phoneReducer.detail };
};

export default connect(mapStateToProps)(Detail_Phone);
