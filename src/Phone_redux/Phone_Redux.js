import React, { Component } from 'react'
import Detail_Phone from './Detail_Phone'
import List_Phone from './List_Phone'

export default class Phone_Redux extends Component {
    render() {
        return (
            <div className='row'>
                <div className="col-6"><List_Phone /></div>
                <div className="col-6"><Detail_Phone /></div>
            </div>
        )
    }
}
