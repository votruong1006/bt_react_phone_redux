import React, { Component } from 'react'
import { connect } from 'react-redux'

class Item_Phone extends Component {
    render() {
        let { tenSP, giaBan, hinhAnh } = { ...this.props.phone }
        return (
            <div className='col-4 p-4 '>
                <div className="card border-primary h-100">
                    <img className="card-img-top" src={hinhAnh} alt />
                    <div className="card-body">
                        <h4 className="card-title">{tenSP}</h4>
                        <p className="card-text">{giaBan}</p>
                        <button onClick={() => { this.props.handleDetail(this.props.phone) }} className='btn btn-success'>Xem chi tiết </button>
                    </div>
                </div>

            </div>
        )
    }
}
let mapDispatchToProps = (dispatch) => {
    return {
        handleDetail: (phone) => {
            let action = {
                type: "DETAIL_PHONE",
                payload: phone
            }
            dispatch(action)
        }
    }
}
export default connect(null, mapDispatchToProps)(Item_Phone)