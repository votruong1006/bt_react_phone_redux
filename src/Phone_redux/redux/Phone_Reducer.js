import { data_phone } from "../data_phone";

let inititalValue = {
    listPhone: data_phone, detail: []
}
export const phoneReducer = (state = inititalValue, action) => {
    switch (action.type) {
        case "DETAIL_PHONE":
            {
                return { ...state, detail: action.payload }
            }


        default: return state

    }
}