import { combineReducers } from "redux";
import { phoneReducer } from "./Phone_Reducer";

export const rootReducer_Phone = combineReducers({
    phoneReducer,
})