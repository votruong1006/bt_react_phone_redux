import React, { Component } from 'react'
import { connect } from 'react-redux'
import Item_Phone from './Item_Phone'

class List_Phone extends Component {
    render() {
        return (
            <div className='row bg-primary'>
                {this.props.listPhone.map((item) => {
                    return <Item_Phone phone={item} />
                })}
            </div>
        )
    }
}


let mapStateToProps = (state) => {
    return {
        listPhone: state.phoneReducer.listPhone
    }
}
export default connect(mapStateToProps)(List_Phone)
